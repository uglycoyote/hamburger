
//
// Node.js script to poll the state of buildbot and send notifications when the build is broken.
//  by Mike Cline
//

/// <reference path="Typings/node/node.d.ts" />

import * as BuildbotInterface from './BuildbotInterface';

import http = require('http');



class BrokenBuildNotifier {

	private config: BuildbotInterface.BuildbotInterfaceConfig = new BuildbotInterface.BuildbotInterfaceConfig();
	private buildbotInterface: BuildbotInterface.BuildbotInterface;


	Initialize() {

		this.config.async = false;
		this.config.runningInBrowser = false;
		this.config.buildbotMaster = "chameleonbuild"
		this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.config);
		
		console.log( "Getting builders" ) 
		this.buildbotInterface.getBuilders(
			function(builderCollection: BuildbotInterface.BuilderCollection) {

				console.log(builderCollection);
				this.builderCollection = builderCollection;	
			},
			function(data) {
				console.log( "failed to get builders: " )
				console.log(data)
			}
		);
	}

}

console.log( "Broken Build Notifier" )
var brokenBuildNotifier = new BrokenBuildNotifier()
brokenBuildNotifier.Initialize()
