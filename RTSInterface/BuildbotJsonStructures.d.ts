// http://json2ts.com/ -- useful tool for creating Typescript interfaces for json data.

declare module BuildbotJsonStructures {
	// JSON response to query to /builders is an object whose keys are builder names and 
	//   values are these objects.
	interface Builder {
		basedir: string;
		cachedBuilds: number[];
		category: string;
		currentBuilds: any[];
		pendingBuilds: number;
		schedulers: string[];
		slaves: string[];
		state: string;
	}

	// JSON structure used by BuildBot for replesenting a file in a changelist.
	export interface File {
		name: string;
	}

	// JSON structure used by BuildBot representing a change in source control (e.g. a Perforce Changelist)
	export interface Change {
		at: string;
		comments: string;
		files: File[];
		number: number;
		rev: string;
		revision: string;
		revlink: string;
		when: number;
		who: string;
	}

	// JSON structure for a BuildBot Source Stamp (which tells you which changelists affected that build)
	export interface SourceStamp {
		changes: Change[];
		revision: string;
	}

	// JSON Structure for a BuildBot build step
	export interface Step {
		hidden: boolean;
		isFinished: boolean;
		isStarted: boolean;
		name: string;
		text: string[];
		times: number[];
		expectations: any[][];
		logs: string[][];
		step_number?: number;
	}

	// JSON response to a /builders/<builderName>/builds?... query is a dictionary object with values 
	//  which are of this type.
	export interface Build {
		blame: string[];
		builderName: string;
		logs: string[][];
		number: number;
		properties: any[][];
		reason: string;
		slave: string;
		sourceStamps: SourceStamp[];
		steps: Step[];
		text: string[];
		times: number[];
	}

	export interface BuildDictionary {
		[buildNumber: number]: Build;
	}


	export interface BuilderDictionary {
		[builderName: string]: Builder;
	}
}