/// <reference path="Typings/react/react-global.d.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", 'react', 'react-dom', './BuildbotInterface'], function (require, exports, React, ReactDOM, BuildbotInterface) {
    "use strict";
    var BuildProps = (function () {
        function BuildProps() {
        }
        return BuildProps;
    }());
    var BuildComponent = (function (_super) {
        __extends(BuildComponent, _super);
        function BuildComponent(props) {
            _super.call(this, props);
        }
        BuildComponent.prototype.render = function () {
            var buildKey = this.props.builder.basedir + "-" + this.props.buildNumber;
            var cachedBuild = this.props.model.cachedBuilds[buildKey];
            var buildDescription = React.createElement("ul", null, "?");
            if (cachedBuild != undefined) {
                var sourceStampsList = cachedBuild.sourceStamps[0].changes.map(function (change) {
                    return React.createElement("li", {key: "change" + change.rev}, change.comments);
                });
                buildDescription = React.createElement("ul", null, sourceStampsList);
            }
            return React.createElement("li", {key: "build" + this.props.buildNumber}, "Build ", this.props.buildNumber, ": ", buildDescription, " ");
        };
        return BuildComponent;
    }(React.Component));
    /** Properties object for the BuilderComponent */
    var BuilderProps = (function () {
        function BuilderProps() {
        }
        return BuilderProps;
    }());
    /** React Component which displays information about a builder */
    var BuilderComponent = (function (_super) {
        __extends(BuilderComponent, _super);
        function BuilderComponent(props) {
            _super.call(this, props);
        }
        BuilderComponent.prototype.render = function () {
            var numBuilds = this.props.config.numRecentBuildsToShow;
            if (this.props.builder.cachedBuilds == undefined) {
                return React.createElement("div", null, "cachedBuilds is undefined");
            }
            var recentBuildNumbers = this.props.builder.cachedBuilds.slice(0, numBuilds);
            var builderComponent = this;
            var builds = recentBuildNumbers.map(function (buildNumber) {
                return React.createElement(BuildComponent, React.__spread({}, builderComponent.props, {key: "build" + buildNumber.toString(), builder: builderComponent.props.builder, buildNumber: buildNumber}));
            });
            return React.createElement("div", null, this.props.builder.basedir, React.createElement("ul", null, builds));
        };
        return BuilderComponent;
    }(React.Component));
    /** Properties for the BuilderCollectionComponent */
    var BuilderCollectionProps = (function () {
        function BuilderCollectionProps() {
        }
        return BuilderCollectionProps;
    }());
    /** React Component which displays a collection of builders */
    var BuilderCollectionComponent = (function (_super) {
        __extends(BuilderCollectionComponent, _super);
        function BuilderCollectionComponent(props) {
            _super.call(this, props);
        }
        BuilderCollectionComponent.prototype.render = function () {
            var builderCollection = this.props.builderCollection;
            var builderCollectionElement = this;
            var builderNamesToShow = builderCollection.getBuilderNames().filter(function (builderName) {
                var builderCategory = builderCollection.builders[builderName].category;
                return (builderCollectionElement.props.categoriesToShow.indexOf(builderCategory) != -1);
            });
            var builderItems = builderNamesToShow.map(function (builderName) {
                var builder = builderCollection.builders[builderName];
                return (React.createElement(BuilderComponent, React.__spread({}, builderCollectionElement.props, {key: builderName, builder: builder})));
            }, builderCollection);
            return React.createElement("div", null, builderItems);
        };
        return BuilderCollectionComponent;
    }(React.Component));
    /**
    * Props class to for the BuildbotResultsTable
    */
    var BuildbotResultsTableProps = (function () {
        function BuildbotResultsTableProps() {
        }
        return BuildbotResultsTableProps;
    }());
    /**
    * Top-level React Component to render the table of Buildbot results..
    */
    var BuildbotResultsTableComponent = (function (_super) {
        __extends(BuildbotResultsTableComponent, _super);
        function BuildbotResultsTableComponent(props) {
            _super.call(this, props);
        }
        BuildbotResultsTableComponent.prototype.render = function () {
            return React.createElement(BuilderCollectionComponent, React.__spread({}, this.props, {builderCollection: this.props.model.builderCollection, categoriesToShow: this.props.config.builderTypesToShow}));
        };
        return BuildbotResultsTableComponent;
    }(React.Component));
    /**
    *  Configuration object used by the BuildbotResultsTableComponent to configure various aspects of how that table is rendered.
    */
    var BuildbotResultsTableConfig = (function () {
        function BuildbotResultsTableConfig() {
            this.builderTypesToShow = ["MainBuilders", "SmokeTest", "LightmapBuilders"];
            this.numRecentBuildsToShow = 10;
        }
        return BuildbotResultsTableConfig;
    }());
    /** Top-level data model structure */
    var BuildbotDataModel = (function () {
        function BuildbotDataModel(changeNotifier) {
            this.interfaceConfig = new BuildbotInterface.BuildbotInterfaceConfig();
            this.cachedBuilds = {};
            this.changeNotifier = changeNotifier;
            this.interfaceConfig.async = true;
            this.interfaceConfig.buildbotMaster = "localhost:8020";
            this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.interfaceConfig);
            var buildbotDataModel = this;
            // Do JSON request for builders 
            this.buildbotInterface.getBuilders(function (builderCollection) {
                //console.log(builderCollection);
                buildbotDataModel.builderCollection = builderCollection;
                buildbotDataModel.requestRecentBuilds(buildbotDataModel.interfaceConfig.initialNumBuildsToRequest);
                buildbotDataModel.onModelChanged();
            }, function (data) {
            });
        }
        BuildbotDataModel.prototype.onModelChanged = function () {
            this.changeNotifier.onModelChanged(this);
        };
        BuildbotDataModel.prototype.requestRecentBuilds = function (numBuildsToRequest) {
            var builderNames = this.builderCollection.getBuilderNames();
            builderNames.forEach(function (builderName) {
                buildbotDataModel.requestRecentBuildsForBuilder(builderName, numBuildsToRequest);
            });
        };
        BuildbotDataModel.prototype.requestRecentBuildsForBuilder = function (builderName, numBuildsToRequest) {
            var builder = this.builderCollection.builders[builderName];
            var buildNumbersToRequest = builder.cachedBuilds.slice(0, numBuildsToRequest);
            buildNumbersToRequest.forEach(function (buildNumber) {
                buildbotDataModel.buildbotInterface.getBuild(builderName, buildNumber, function (builldDictionary) {
                    //console.log(builldDictionary);
                    var builderNameAndNumber = builderName + "-" + buildNumber;
                    buildbotDataModel.cachedBuilds[builderNameAndNumber] = builldDictionary[buildNumber];
                    buildbotDataModel.onModelChanged();
                }, function (data) {
                });
            });
        };
        return BuildbotDataModel;
    }());
    /** Concrete implementation of the Notification interface, for BuildbotDataModel to talk to the React presentation layer. */
    var BuildbotReactDataModelChangeNotify = (function () {
        function BuildbotReactDataModelChangeNotify(config) {
            this.config = config;
        }
        BuildbotReactDataModelChangeNotify.prototype.onModelChanged = function (model) {
            //console.log( "In BuildbotReactDataModelChangeNotify, model = " )
            //console.log(model);
            ReactDOM.render(React.createElement(BuildbotResultsTableComponent, {model: buildbotDataModel, config: this.config}), document.getElementsByClassName('buildbotInterface')[0]);
        };
        return BuildbotReactDataModelChangeNotify;
    }());
    var buildbotResultsTableConfig = new BuildbotResultsTableConfig();
    var buildbotReactChangeNotifier = new BuildbotReactDataModelChangeNotify(buildbotResultsTableConfig);
    var buildbotDataModel = new BuildbotDataModel(buildbotReactChangeNotifier);
});
//# sourceMappingURL=BuildbotReact.js.map