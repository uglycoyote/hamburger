var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './tsUnit', './BuildbotInterface'], function (require, exports, tsUnit, BuildbotInterface) {
    "use strict";
    //require(['tsUnit', 'BuildBotInterface']);
    function waitfor_async(test, msec, successCallback, timeoutCallback) {
        var interval = 10; // how often to check condition, ms.
        if (msec < 0) {
            timeoutCallback();
            return;
        }
        if (test()) {
            successCallback();
            return;
        }
        var remainingTime = msec - interval;
        setTimeout(function () { waitfor_async(test, remainingTime, successCallback, timeoutCallback); });
    }
    function sleepFor(sleepDuration) {
        var now = new Date().getTime();
        while (new Date().getTime() < now + sleepDuration) { }
    }
    function waitfor(test, msec, successCallback, timeoutCallback) {
        var remainingTime = msec;
        var interval = 10;
        while (!test() && remainingTime > 0) {
            sleepFor(interval);
            remainingTime -= interval;
        }
        if (remainingTime <= 0) {
            console.log("timeoutCallback");
            timeoutCallback();
            return;
        }
        successCallback();
    }
    var BuildbotInterfaceTests = (function (_super) {
        __extends(BuildbotInterfaceTests, _super);
        function BuildbotInterfaceTests() {
            _super.call(this);
            this.config = new BuildbotInterface.BuildbotInterfaceConfig();
            this.config.async = false;
            this.config.buildbotMaster = "localhost:8020";
            this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.config);
        }
        // Tests that BuildbotInterface.getBuilders returns something.
        BuildbotInterfaceTests.prototype.GetBuilderCollection = function () {
            var testClass = this;
            this.buildbotInterface.getBuilders(function (builderCollection) {
                console.log(builderCollection);
                testClass.builderCollection = builderCollection;
                //testClass.fail( "testing" )
            }, function (data) {
                testClass.fail("ajax request to get builder names failed");
            });
            waitfor(function () { console.log("testing"); return testClass.builderCollection != null; }, 1000, function () { console.log("BuilderColleciton has been set to " + testClass.builderCollection); }, function () { testClass.fail("timed out waiting for builderCollection"); });
            if (testClass.builderCollection == null) {
                testClass.fail("testClass.builderCollection was supposed to have been set.");
            }
        };
        // Tests that BuilderCollection::getBuilderNames returns some builders.
        BuildbotInterfaceTests.prototype.EnsureBuilderCollectionHasBuilders = function () {
            console.log("starting second test");
            var testClass = this;
            if (testClass.builderCollection == null) {
                testClass.fail("testClass.builderCollection was supposed to have been set by previous test.");
            }
            if (this.builderCollection.getBuilderNames().length == 0) {
                testClass.fail("There were no builder names returned in the list.");
            }
        };
        // Gets the first builder and makes sure it is non-null.
        BuildbotInterfaceTests.prototype.GetFirstBuilder = function () {
            this.firstBuilderName = this.builderCollection.getBuilderNames()[0];
            this.firstBuilder = this.builderCollection.builders[this.firstBuilderName];
            if (this.firstBuilder == undefined) {
                this.fail();
            }
        };
        BuildbotInterfaceTests.prototype.EnsureFirstBuilderHasBuilds = function () {
            if (this.firstBuilder.cachedBuilds.length == 0) {
                this.fail();
            }
        };
        BuildbotInterfaceTests.prototype.GetFirstBuildOfFirstBuilder = function () {
            var testClass = this;
            var firstCachedBuildNumber = this.firstBuilder.cachedBuilds[0];
            this.buildbotInterface.getBuild(this.firstBuilderName, firstCachedBuildNumber, function (buildDict) {
                var buildKeys = Object.keys(buildDict);
                if (buildKeys.length != 1 || buildKeys[0] != firstCachedBuildNumber.toString()) {
                    testClass.fail("expected the query for one build to return a dictionary with one key, which is the build number that was requested");
                }
                testClass.firstBuild = buildDict[buildKeys[0]];
                console.log(testClass.firstBuild);
            }, function (data) {
                testClass.fail("ajax request to get builder names failed");
            });
        };
        BuildbotInterfaceTests.prototype.BuildShouldHaveABlameList = function () {
            if (this.firstBuild.blame == undefined || this.firstBuild.blame.length == 0) {
                this.fail("expected the build to have a blame list with at least one element");
            }
        };
        BuildbotInterfaceTests.prototype.BuildShouldHaveSourceStamps = function () {
            if (this.firstBuild.sourceStamps == undefined || this.firstBuild.sourceStamps.length == 0) {
                this.fail("expected the build to have a sourceStamps list with at least one element");
            }
            this.firstSourceStamp = this.firstBuild.sourceStamps[0];
            console.log(this.firstSourceStamp);
        };
        BuildbotInterfaceTests.prototype.SourceStampsShouldHaveJustOneChange = function () {
            if (this.firstSourceStamp.changes == undefined || this.firstSourceStamp.changes.length != 1) {
                this.fail("expected the sourceStamp to have just one change");
            }
            this.firstChange = this.firstSourceStamp.changes[0];
            console.log(this.firstChange);
        };
        BuildbotInterfaceTests.prototype.PerforceChangeCommentsAlwaysStartWithTheWordChange = function () {
            if (this.firstChange.comments.indexOf("Change") != 0) {
                this.fail("Expected the comments field of a change to start with the word Change, but found : " + this.firstChange.comments);
            }
        };
        return BuildbotInterfaceTests;
    }(tsUnit.TestClass));
    exports.BuildbotInterfaceTests = BuildbotInterfaceTests;
});
//# sourceMappingURL=TestBuildbotInterface.js.map