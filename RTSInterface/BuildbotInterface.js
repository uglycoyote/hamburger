/// <reference path="Typings/jquery/jquery.d.ts" />
/// <reference path="BuildbotJsonStructures.d.ts" />
define(["require", "exports"], function (require, exports) {
    "use strict";
    //import $ = require('jquery');
    //import http = require('http');  // uncomment if building for node
    var BuildbotInterfaceConfig = (function () {
        function BuildbotInterfaceConfig() {
            // Whether to do ajax queries asynchronously.  This is true by default
            //  but turned off for testing because the test suite requires synchronous behaviour.
            this.async = true;
            this.runningInBrowser = true;
            this.initialNumBuildsToRequest = 5;
        }
        return BuildbotInterfaceConfig;
    }());
    exports.BuildbotInterfaceConfig = BuildbotInterfaceConfig;
    var BuilderCollection = (function () {
        function BuilderCollection() {
        }
        BuilderCollection.prototype.getBuilderNames = function () {
            return Object.keys(this.builders);
        };
        return BuilderCollection;
    }());
    exports.BuilderCollection = BuilderCollection;
    var BuildbotInterface = (function () {
        function BuildbotInterface(buildbotInterfaceConfig) {
            this.config = buildbotInterfaceConfig;
        }
        BuildbotInterface.prototype.test = function (x) {
            x = x + 5;
            return x;
        };
        BuildbotInterface.prototype.getJSON_Node = function (url, success, failure) {
            // http.get(url, function(res) {
            // 	var body = '';
            // 	res.on('data', function(chunk) {
            // 		body += chunk;
            // 	});
            // 	res.on('end', function() {
            // 		var json = JSON.parse(body);
            // 		console.log("Got a response: ", json);
            // 		success(json)
            // 	});
            // }).on('error', function(e) {
            // 	console.log("Got an error: ", e);
            // });
        };
        BuildbotInterface.prototype.getJSON_JQuery = function (url, success, failure) {
            $.ajax({ url: url, async: this.config.async }).then(success, failure);
        };
        BuildbotInterface.prototype.getJSON_NonJQuery = function (url, success, failure) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url); // tried using this.config.async as third paarm, but that seems to cause it to ignore the request altogether
            xhr.send(null);
            xhr.onreadystatechange = function () {
                var DONE = 4; // readyState 4 means the request is done.
                var OK = 200; // status 200 is a successful return.
                if (xhr.readyState === DONE) {
                    if (xhr.status === OK) {
                        var json = JSON.parse(xhr.responseText);
                        //console.log(xhr.responseText); // 'This is the returned text.'
                        success(json);
                    }
                    else {
                        console.log('Error: ' + xhr.status); // An error occurred during the request.
                        failure(xhr.status);
                    }
                }
            };
        };
        BuildbotInterface.prototype.getJSON = function (url, success, failure) {
            if (this.config.runningInBrowser) {
                this.getJSON_NonJQuery(url, success, failure);
            }
            else {
                this.getJSON_Node(url, success, failure);
            }
        };
        // Performs a /api/v2/builders request to the BuildBot master, which returns a BuilderDictionary
        BuildbotInterface.prototype.getBuilders = function (successCallback, failureCallback) {
            var url = "http://" + this.config.buildbotMaster + "/api/v2/builders";
            var builderInfo = new BuilderCollection();
            this.getJSON(url, function (data) {
                console.log(data);
                builderInfo.builders = data;
                console.log(builderInfo.getBuilderNames());
                successCallback(builderInfo);
            }, function (data) {
                failureCallback(data);
            });
        };
        // Performs a /api/v2/builders/<BuilderName>/builds?select=<buildNumber> query, which returns a Build object
        BuildbotInterface.prototype.getBuild = function (builderName, buildNumber, successCallback, failureCallback) {
            var url = "http://" + this.config.buildbotMaster + "/api/v2/builders/" + builderName + "/builds?select=" + buildNumber;
            this.getJSON(url, function (build) {
                //console.log(build);
                successCallback(build);
            }, function (data) {
                failureCallback(data);
            });
        };
        return BuildbotInterface;
    }());
    exports.BuildbotInterface = BuildbotInterface;
});
//# sourceMappingURL=BuildbotInterface.js.map