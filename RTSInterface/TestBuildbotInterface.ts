import * as tsUnit from './tsUnit';
import * as BuildbotInterface from './BuildbotInterface';

//require(['tsUnit', 'BuildBotInterface']);

function waitfor_async(test : () => boolean, msec : number,
	successCallback : () => void , timeoutCallback : () => void) {
    
	var interval = 10; // how often to check condition, ms.
	if (msec < 0) { timeoutCallback(); return; }
	if (test()) { successCallback(); return; }
	var remainingTime = msec - interval;
	setTimeout(function() { waitfor_async(test, remainingTime, successCallback, timeoutCallback) });
}

function sleepFor(sleepDuration) {
    var now = new Date().getTime();
    while (new Date().getTime() < now + sleepDuration) { /* do nothing */ }
}

function waitfor(test: () => boolean, msec: number,
	successCallback: () => void, timeoutCallback: () => void) {

	var remainingTime = msec;
	var interval = 10;

	while(!test() && remainingTime > 0 )
	{
		sleepFor(interval);
		remainingTime -= interval;
	}

	if ( remainingTime <= 0 )
	{
		console.log("timeoutCallback")
		timeoutCallback()
		return;
	}

	successCallback();
}


export class BuildbotInterfaceTests extends tsUnit.TestClass {

	private config: BuildbotInterface.BuildbotInterfaceConfig = new BuildbotInterface.BuildbotInterfaceConfig();
	private buildbotInterface: BuildbotInterface.BuildbotInterface;

	private builderCollection: BuildbotInterface.BuilderCollection;
	private firstBuilderName: string;
	private firstBuilder: BuildbotJsonStructures.Builder;
	private firstBuild: BuildbotJsonStructures.Build;
	private firstSourceStamp: BuildbotJsonStructures.SourceStamp;
	private firstChange: BuildbotJsonStructures.Change;

	constructor() {
		super();

		this.config.async = false;
		this.config.buildbotMaster = "localhost:8020"
		this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.config);
	}


	// Tests that BuildbotInterface.getBuilders returns something.
	GetBuilderCollection() {
		var testClass = this;

		this.buildbotInterface.getBuilders(
			function(builderCollection: BuildbotInterface.BuilderCollection) {

				console.log(builderCollection);
				testClass.builderCollection = builderCollection;

				//testClass.fail( "testing" )
			},
			function(data) {
				testClass.fail("ajax request to get builder names failed");
			}
		);

		waitfor(
			function() { console.log("testing"); return testClass.builderCollection != null; },
			1000,
			function() { console.log("BuilderColleciton has been set to " + testClass.builderCollection) },
			function() { testClass.fail("timed out waiting for builderCollection") } )

		if (testClass.builderCollection == null) {
			testClass.fail("testClass.builderCollection was supposed to have been set.")
		}

	}

	// Tests that BuilderCollection::getBuilderNames returns some builders.
	EnsureBuilderCollectionHasBuilders() {
		console.log("starting second test")
		var testClass = this;
		if (testClass.builderCollection==null){
			testClass.fail("testClass.builderCollection was supposed to have been set by previous test.")
		}

		if (this.builderCollection.getBuilderNames().length == 0) {
			testClass.fail("There were no builder names returned in the list.");
		}

	}

	// Gets the first builder and makes sure it is non-null.
	GetFirstBuilder() {
		this.firstBuilderName = this.builderCollection.getBuilderNames()[0]
		this.firstBuilder = this.builderCollection.builders[this.firstBuilderName]
		if (this.firstBuilder == undefined) {
			this.fail();
		}
	}

	EnsureFirstBuilderHasBuilds() {
		if (this.firstBuilder.cachedBuilds.length == 0) {
			this.fail();
		}
	}

	GetFirstBuildOfFirstBuilder() {
		var testClass = this;
		var firstCachedBuildNumber = this.firstBuilder.cachedBuilds[0];
		this.buildbotInterface.getBuild( this.firstBuilderName, firstCachedBuildNumber,
			function(buildDict: BuildbotJsonStructures.BuildDictionary) {
				var buildKeys = Object.keys(buildDict);
				if (buildKeys.length != 1 || buildKeys[0] != firstCachedBuildNumber.toString()) {
					testClass.fail( "expected the query for one build to return a dictionary with one key, which is the build number that was requested" ) 
				}

				testClass.firstBuild = buildDict[buildKeys[0]];
				console.log(testClass.firstBuild);
			},
			function(data) {
				testClass.fail("ajax request to get builder names failed");
			}
		);
	}

	BuildShouldHaveABlameList() {
		if ( this.firstBuild.blame == undefined || this.firstBuild.blame.length == 0 )
		{
			this.fail("expected the build to have a blame list with at least one element");
		}
	}

	BuildShouldHaveSourceStamps() {
		if (this.firstBuild.sourceStamps == undefined || this.firstBuild.sourceStamps.length == 0) {
			this.fail("expected the build to have a sourceStamps list with at least one element");
		}

		this.firstSourceStamp = this.firstBuild.sourceStamps[0]
		console.log(this.firstSourceStamp);
	}

	SourceStampsShouldHaveJustOneChange() {
		if (this.firstSourceStamp.changes == undefined || this.firstSourceStamp.changes.length != 1) {
			this.fail("expected the sourceStamp to have just one change");
		}

		this.firstChange = this.firstSourceStamp.changes[0]
		console.log(this.firstChange);
	}

	PerforceChangeCommentsAlwaysStartWithTheWordChange()
	{
		if (this.firstChange.comments.indexOf("Change") != 0) {
			this.fail("Expected the comments field of a change to start with the word Change, but found : " + this.firstChange.comments);
		}
	}


}

