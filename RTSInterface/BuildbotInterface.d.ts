/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="buildbotJsonStructures.d.ts" />
export declare class BuildbotInterfaceConfig {
    buildbotMaster: string;
    async: boolean;
    runningInBrowser: boolean;
    initialNumBuildsToRequest: number;
    constructor();
}
export declare class BuilderCollection {
    builders: BuildbotJsonStructures.BuilderDictionary;
    getBuilderNames(): Array<string>;
}
export declare class BuildbotInterface {
    config: BuildbotInterfaceConfig;
    constructor(buildbotInterfaceConfig: BuildbotInterfaceConfig);
    test(x: number): number;
    getJSON_Node(url: any, success: any, failure: any): void;
    getJSON_JQuery(url: any, success: any, failure: any): void;
    getJSON_NonJQuery(url: any, success: any, failure: any): void;
    getJSON(url: any, success: any, failure: any): void;
    getBuilders(successCallback: ((b: BuilderCollection) => void), failureCallback: (data: any) => void): void;
    getBuild(builderName: any, buildNumber: any, successCallback: ((b: BuildbotJsonStructures.BuildDictionary) => void), failureCallback: (data: any) => void): void;
}
