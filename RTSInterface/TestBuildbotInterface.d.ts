import * as tsUnit from './tsUnit';
export declare class BuildbotInterfaceTests extends tsUnit.TestClass {
    private config;
    private buildbotInterface;
    private builderCollection;
    private firstBuilderName;
    private firstBuilder;
    private firstBuild;
    private firstSourceStamp;
    private firstChange;
    constructor();
    GetBuilderCollection(): void;
    EnsureBuilderCollectionHasBuilders(): void;
    GetFirstBuilder(): void;
    EnsureFirstBuilderHasBuilds(): void;
    GetFirstBuildOfFirstBuilder(): void;
    BuildShouldHaveABlameList(): void;
    BuildShouldHaveSourceStamps(): void;
    SourceStampsShouldHaveJustOneChange(): void;
    PerforceChangeCommentsAlwaysStartWithTheWordChange(): void;
}
