/// <reference path="Typings/jquery/jquery.d.ts" />
/// <reference path="BuildbotJsonStructures.d.ts" />

//import $ = require('jquery');
//import http = require('http');  // uncomment if building for node

export class BuildbotInterfaceConfig {

	// The name of the machine running the buildbot master software.
	buildbotMaster: string;

	// Whether to do ajax queries asynchronously.  This is true by default
	//  but turned off for testing because the test suite requires synchronous behaviour.
	async: boolean = true;

	runningInBrowser: boolean = true;

	initialNumBuildsToRequest: number = 5;

	constructor() {
	}

}


export class BuilderCollection {

	builders: BuildbotJsonStructures.BuilderDictionary;

	getBuilderNames(): Array<string> {
		return Object.keys(this.builders);
	}

}

export class BuildbotInterface 
{

	config: BuildbotInterfaceConfig;

	constructor(buildbotInterfaceConfig : BuildbotInterfaceConfig) {
		this.config = buildbotInterfaceConfig;
	}

	test(x: number): number {
		x = x + 5;
		return x;
	}


	getJSON_Node(url, success, failure) {
		// http.get(url, function(res) {
		// 	var body = '';

		// 	res.on('data', function(chunk) {
		// 		body += chunk;
		// 	});

		// 	res.on('end', function() {
		// 		var json = JSON.parse(body);
		// 		console.log("Got a response: ", json);
		// 		success(json)
		// 	});
		// }).on('error', function(e) {
		// 	console.log("Got an error: ", e);
		// });
	}

	getJSON_JQuery(url, success, failure) {
		$.ajax({ url: url, async: this.config.async } ).then(
			success, failure )
	}

	getJSON_NonJQuery(url, success, failure) {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url); // tried using this.config.async as third paarm, but that seems to cause it to ignore the request altogether
		xhr.send(null);

		xhr.onreadystatechange = function() {
			var DONE = 4; // readyState 4 means the request is done.
			var OK = 200; // status 200 is a successful return.
			if (xhr.readyState === DONE) {
				if (xhr.status === OK){
					var json = JSON.parse( xhr.responseText )
					//console.log(xhr.responseText); // 'This is the returned text.'
					success(json)
				}
				else {
					console.log('Error: ' + xhr.status); // An error occurred during the request.
					failure(xhr.status)
				}
			} 
		};
	

	}

	getJSON(url, success, failure) {
		if (this.config.runningInBrowser) {
			this.getJSON_NonJQuery(url, success, failure);
		}
		else {
			this.getJSON_Node(url, success, failure);
		}

	}


	// Performs a /api/v2/builders request to the BuildBot master, which returns a BuilderDictionary
	getBuilders( 
		successCallback : ((b : BuilderCollection)=>void), 
		failureCallback : (data:any)=>void )
	{
		var url = "http://" + this.config.buildbotMaster + "/api/v2/builders"

		var builderInfo = new BuilderCollection();

		this.getJSON(url,
			function(data : BuildbotJsonStructures.BuilderDictionary) {

				console.log(data);
				builderInfo.builders = data;

				console.log(builderInfo.getBuilderNames());

				successCallback(builderInfo);


			},
			function(data) {
				failureCallback(data)
			}
		)
	}

	// Performs a /api/v2/builders/<BuilderName>/builds?select=<buildNumber> query, which returns a Build object
	getBuild( builderName, buildNumber, 
		successCallback: ((b: BuildbotJsonStructures.BuildDictionary) => void),
		failureCallback: (data: any) => void) {
		var url = "http://" + this.config.buildbotMaster + "/api/v2/builders/" + builderName + "/builds?select=" + buildNumber;

		this.getJSON( url,
			function(build: BuildbotJsonStructures.BuildDictionary) {
				//console.log(build);
				successCallback(build);
			},
			function(data) {
				failureCallback(data)
			}
		)
	}



}


