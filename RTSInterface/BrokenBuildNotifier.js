//
// Node.js script to poll the state of buildbot and send notifications when the build is broken.
//  by Mike Cline
//
define(["require", "exports", './BuildbotInterface'], function (require, exports, BuildbotInterface) {
    "use strict";
    var BrokenBuildNotifier = (function () {
        function BrokenBuildNotifier() {
            this.config = new BuildbotInterface.BuildbotInterfaceConfig();
        }
        BrokenBuildNotifier.prototype.Initialize = function () {
            this.config.async = false;
            this.config.runningInBrowser = false;
            this.config.buildbotMaster = "chameleonbuild";
            this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.config);
            console.log("Getting builders");
            this.buildbotInterface.getBuilders(function (builderCollection) {
                console.log(builderCollection);
                this.builderCollection = builderCollection;
            }, function (data) {
                console.log("failed to get builders: ");
                console.log(data);
            });
        };
        return BrokenBuildNotifier;
    }());
    console.log("Broken Build Notifier");
    var brokenBuildNotifier = new BrokenBuildNotifier();
    brokenBuildNotifier.Initialize();
});
//# sourceMappingURL=BrokenBuildNotifier.js.map