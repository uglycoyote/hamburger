/// <reference path="Typings/react/react-global.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as BuildbotInterface from './BuildbotInterface';


class BuildProps {
	key: string;
	builder: BuildbotJsonStructures.Builder;
	buildNumber: number;
	config: BuildbotResultsTableConfig;
	model: BuildbotDataModel;
}

class BuildComponent extends React.Component<BuildProps, any> {
	constructor(props: BuildProps) {
		super(props)
	}

	render() {

		var buildKey = this.props.builder.basedir + "-" + this.props.buildNumber;
		var cachedBuild = this.props.model.cachedBuilds[buildKey]
		var buildDescription: JSX.Element = <ul>?</ul>

		if (cachedBuild != undefined) {

			var sourceStampsList = cachedBuild.sourceStamps[0].changes.map(
				function(change) {
					return <li key={"change" + change.rev}>{change.comments}</li>
				}
			);

			buildDescription = <ul>{sourceStampsList}</ul>
		}

		return <li key={"build" + this.props.buildNumber}>Build {this.props.buildNumber}: {buildDescription} </li>
	}

}

/** Properties object for the BuilderComponent */
class BuilderProps {
	builder: BuildbotJsonStructures.Builder;
	key: string;

	config: BuildbotResultsTableConfig;
	model: BuildbotDataModel;
}

/** React Component which displays information about a builder */
class BuilderComponent extends React.Component<BuilderProps, any> {
	constructor(props: BuilderProps) {
		super(props)
	}
	render() {

		var numBuilds = this.props.config.numRecentBuildsToShow;
		if (this.props.builder.cachedBuilds == undefined) {
			return <div>cachedBuilds is undefined</div>
		}

		var recentBuildNumbers = this.props.builder.cachedBuilds.slice(0, numBuilds);

		var builderComponent = this;

		var builds = recentBuildNumbers.map(
			function(buildNumber) {
				return <BuildComponent {...builderComponent.props} 
					key={"build" + buildNumber.toString()}
					builder={builderComponent.props.builder}
					buildNumber={buildNumber} />
			}
		)

		return <div>
			{this.props.builder.basedir}
			<ul>
			{builds}
			</ul>
			</div>
	}
}

/** Properties for the BuilderCollectionComponent */
class BuilderCollectionProps {
	builderCollection: BuildbotInterface.BuilderCollection;
	categoriesToShow: string[];
	config: BuildbotResultsTableConfig;
	model: BuildbotDataModel;
}

/** React Component which displays a collection of builders */
class BuilderCollectionComponent extends React.Component<BuilderCollectionProps, any> {
	constructor(props: BuilderCollectionProps) {
		super(props);
	}
	render() {

		var builderCollection = this.props.builderCollection;
		var builderCollectionElement = this;

		var builderNamesToShow = builderCollection.getBuilderNames().filter(
			function(builderName) {
				var builderCategory = builderCollection.builders[builderName].category;
				return ( builderCollectionElement.props.categoriesToShow.indexOf(builderCategory) != -1 )
			});

		var builderItems = builderNamesToShow.map(function(builderName) {
			var builder : BuildbotJsonStructures.Builder = builderCollection.builders[builderName];
			return (
				<BuilderComponent {...builderCollectionElement.props} key={builderName} builder={builder}/>
			);
		}, builderCollection);

		return <div>{builderItems}</div>
	}
}

/**
* Props class to for the BuildbotResultsTable
*/
class BuildbotResultsTableProps {
	model: BuildbotDataModel;
	config: BuildbotResultsTableConfig;
}

/**
* Top-level React Component to render the table of Buildbot results..
*/
class BuildbotResultsTableComponent extends React.Component<BuildbotResultsTableProps, any>
{
	constructor(props: BuildbotResultsTableProps) {
		super(props);
	}
	render() {

		return <BuilderCollectionComponent {...this.props} builderCollection={this.props.model.builderCollection} categoriesToShow={this.props.config.builderTypesToShow}/>
	}
}

/**
*  Configuration object used by the BuildbotResultsTableComponent to configure various aspects of how that table is rendered.
*/
class BuildbotResultsTableConfig {
	builderTypesToShow = ["MainBuilders", "SmokeTest", "LightmapBuilders"];

	numRecentBuildsToShow = 10;
}

/**
* Interface used by the BuildbotDataModel to notify presentation layer about changes to the data model without
*   having a code dependency on the presentation layer.
*/
interface IBuildbotDataModelChangeNotify {

	onModelChanged : ( model : BuildbotDataModel ) => void	
}

/**
* Dictionary type that maps a name like "pc-build-39466" to the information about build 39466 on pc-build.
*/
interface CachedBuildDictionary {
	[builderNameAndNumber: string]: BuildbotJsonStructures.Build;
}


/** Top-level data model structure */
class BuildbotDataModel {
	private interfaceConfig: BuildbotInterface.BuildbotInterfaceConfig = new BuildbotInterface.BuildbotInterfaceConfig();
	private buildbotInterface: BuildbotInterface.BuildbotInterface;

	public builderCollection: BuildbotInterface.BuilderCollection;

	public changeNotifier: IBuildbotDataModelChangeNotify;

	public cachedBuilds: CachedBuildDictionary = {};

	constructor(changeNotifier: IBuildbotDataModelChangeNotify) {
		this.changeNotifier = changeNotifier;
		this.interfaceConfig.async = true;
		this.interfaceConfig.buildbotMaster = "localhost:8020"
		this.buildbotInterface = new BuildbotInterface.BuildbotInterface(this.interfaceConfig);
		var buildbotDataModel = this;

		// Do JSON request for builders 
		this.buildbotInterface.getBuilders(
			function(builderCollection: BuildbotInterface.BuilderCollection) {

				//console.log(builderCollection);
				buildbotDataModel.builderCollection = builderCollection;

				buildbotDataModel.requestRecentBuilds(buildbotDataModel.interfaceConfig.initialNumBuildsToRequest);

				buildbotDataModel.onModelChanged( );
			},
			function(data) {
			}
		);

	}

	onModelChanged( ) {
		this.changeNotifier.onModelChanged(this);
	}

	requestRecentBuilds(numBuildsToRequest: number) {

		var builderNames = this.builderCollection.getBuilderNames();

		builderNames.forEach( 
			function(builderName) {
				buildbotDataModel.requestRecentBuildsForBuilder(builderName, numBuildsToRequest);
			}
			)
	}

	requestRecentBuildsForBuilder(builderName: string, numBuildsToRequest: number)
	{ 
		var builder = this.builderCollection.builders[builderName];
		var buildNumbersToRequest = builder.cachedBuilds.slice(0, numBuildsToRequest);		
		buildNumbersToRequest.forEach( 
			function(buildNumber) {


				buildbotDataModel.buildbotInterface.getBuild(builderName, buildNumber,
					function(builldDictionary : BuildbotJsonStructures.BuildDictionary) {

						//console.log(builldDictionary);
						var builderNameAndNumber = builderName + "-" + buildNumber;
						buildbotDataModel.cachedBuilds[ builderNameAndNumber ] = builldDictionary[buildNumber]

						buildbotDataModel.onModelChanged();
					},
					function(data) {
					}
				);
			}
			)
	}
}

/** Concrete implementation of the Notification interface, for BuildbotDataModel to talk to the React presentation layer. */
class BuildbotReactDataModelChangeNotify implements IBuildbotDataModelChangeNotify
{
	config: BuildbotResultsTableConfig;

	constructor(config: BuildbotResultsTableConfig) {
		this.config = config;
	}

	onModelChanged(model: BuildbotDataModel ) 
	{
		//console.log( "In BuildbotReactDataModelChangeNotify, model = " )
		//console.log(model);

		ReactDOM.render(
			<BuildbotResultsTableComponent model={buildbotDataModel} config={this.config}/>,
			document.getElementsByClassName('buildbotInterface')[0]
		);

	}
}

var buildbotResultsTableConfig = new BuildbotResultsTableConfig();

var buildbotReactChangeNotifier = new BuildbotReactDataModelChangeNotify(buildbotResultsTableConfig);
var buildbotDataModel = new BuildbotDataModel( buildbotReactChangeNotifier );

