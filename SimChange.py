
import os
import time

def ChangeLocalFile( filename ):
	timeString = time.strftime("%c")

	outfile = open( filename, "a" )
	outfile.write(timeString + "\n")
	outfile.close


def GitCommitEverything():
	os.system( "git add -A .")
	timeString = time.strftime("%c")
	message = "Automated test change from " + timeString
	os.system( 'git commit -m "{}"'.format(message) )
	os.system( 'git push')

ChangeLocalFile( "TestFile.txt" )
GitCommitEverything()