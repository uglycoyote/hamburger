set -o verbose
mkdir out
cd out

git clone git://github.com/buildbot/buildbot.git buildbot-src

virtualenv hamburger-sandbox
source hamburger-sandbox/bin/activate

cd buildbot-src
pip install -e master
pip install -e slave
make frontend

cd ..
cd hamburger-sandbox
buildbot create-master master
buildbot-worker create-worker worker1 localhost:9999 worker1 pass
buildbot-worker create-worker worker2 localhost:9989 worker2 pass
buildbot-worker create-worker worker3 localhost:9989 worker3 pass

cp ../../master.cfg master


